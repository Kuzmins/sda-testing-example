package sda.software.testing;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class CalculatorJunit4Test {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private Calculator calculator = new Calculator();

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfDivideByZero() {
        calculator.divide(10, 0);
    }

    @Test
    public void shouldThrowExceptionIfDivideByZero2() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Divide by 0");

        calculator.divide(10, 0);
    }
}