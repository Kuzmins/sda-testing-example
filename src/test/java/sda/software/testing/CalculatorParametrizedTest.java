package sda.software.testing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;
import org.junit.jupiter.params.provider.*;

import java.time.Month;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorParametrizedTest {
    private Calculator calculator = new Calculator();

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 1000, -1, -1000})
    void shouldReturnZeroIfMultiplyByZero(int argument) {
        int result = calculator.multiply(argument, 0);

        assertEquals(0, result);
    }

    @ParameterizedTest(name = "{0} should have at least 30 days")
    @EnumSource(value = Month.class, names = {"APRIL", "JUNE", "SEPTEMBER", "NOVEMBER"}, mode = EnumSource.Mode.INCLUDE)
    void someMonthEnums_Are30DaysLong(Month month) {
        final boolean isALeapYear = false;
        assertEquals(30, month.length(isALeapYear));
    }

    @ParameterizedTest(name = "{0} plus {1} should be {2}")
    @CsvSource({"1, 2, 3", "-1, -1, -2", "-10, 10, 0"})
    void shouldSumTwoValues(int first, int second, int expectedResult) {
        int result = calculator.add(first, second);

        assertEquals(expectedResult, result);
    }

    @ParameterizedTest(name = "{0} max {1} should be {2}")
    @CsvSource({"1, 2, 2", "-1, -1, -1", "-10, 10, 10"})
    void maximumOfTwoNumber(int first, int second, int result) {
        int maxNumber = Math.max(first, second);

        assertEquals(result, maxNumber);
    }

    @ParameterizedTest(name = "{0} multiply {1} should be {2}")
    @CsvFileSource(resources = "/testData.csv")
    void shouldMultiplyTwoValues(int first, int second, int expectedResult) {
        int result = calculator.multiply(first, second);

        assertEquals(expectedResult, result);
    }

    @ParameterizedTest(name = "{0} subtract {1} should be {2}")
    @MethodSource("getParameters")
    void shouldSubtractTwoValues(@ConvertWith(HexToInt.class) int first, int second, int expectedResult) {
        int result = calculator.sub(first, second);

        assertEquals(expectedResult, result);
    }

    static Stream<Arguments> getParameters() {
        return Stream.of(
                Arguments.of("A", 5, 5)
        );
    }

    @ParameterizedTest(name = "Test with {0}")
    @MethodSource("getParameters2")
    void shouldSubtractTwoValues2(Params params) {
        int result = calculator.sub(params.first, params.second);

        assertEquals(params.result, result);
    }
     static List<Params> getParameters2() {
        return Arrays.asList(
                new Params(10, 5, 5),
                new Params(-10, -10, 0),
                new Params(0, 0, 0)
        );
    }


    static class Params {
        int first;
        int second;
        int result;

        public Params(int first, int second, int result) {
            this.first = first;
            this.second = second;
            this.result = result;
        }
    }


    static class HexToInt extends SimpleArgumentConverter {
        @Override
        protected Object convert(Object o, Class<?> targetType) {
            assertEquals(int.class, targetType, "Can only convert to int");
            return Integer.decode("0x" + o.toString());
        }
    }


}