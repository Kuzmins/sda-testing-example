package sda.software.testing;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private Calculator calculator = new Calculator();

    @Test
    void shouldAddTwoNumbers() {
        int result = calculator.add(2, 2);

        assertEquals(4, result);
    }

    @Test
    void shouldThrowExceptionIfDivideByZero() {
        try {
            calculator.divide(10, 0);
            fail("Exception was not thrown!");
        } catch (IllegalArgumentException e) {
            assertEquals("Divide by 0", e.getMessage());
        }
    }

    @Test
    void shouldThrowExceptionIfDivideByZero2() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            calculator.divide(10, 0);
        });

        assertEquals("Divide by 0", exception.getMessage());
    }

    @Test
    void shouldThrowExceptionIfDivideByZero3() {
        IllegalArgumentException exception = catchThrowableOfType(() -> {
            calculator.divide(10, 0);
        }, IllegalArgumentException.class);

        assertEquals("Divide by 0", exception.getMessage());

        // Another solution
        Throwable throwable = catchThrowable(() -> {
            calculator.divide(10, 0);
        });

        assertTrue(throwable instanceof IllegalArgumentException);
        assertEquals("Divide by 0", exception.getMessage());
    }

    @Test
    void shouldThrowExceptionIfDivideByZero4() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> {
                    calculator.divide(10, 0);
                })
                .withMessage("Divide by 0")
                .withNoCause();

        //another way
        assertThatThrownBy(()-> calculator.divide(10, 0))
                .withFailMessage("Divide by 0")
                .isExactlyInstanceOf(IllegalArgumentException.class);

    }
}